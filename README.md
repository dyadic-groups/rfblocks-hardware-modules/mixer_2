# A PE4140B Quad FET Mixer

## Features

- Good broadband performance. 50 to 5000 MHz for both up- and
  down-conversion.
- High linearity: nominal IIP3 of 25 dBm.
- Nominal conversion loss of 6.5 dB.
- Nominal 35 dB LO to RF isolation between 500 and 2000 MHz

## Documentation

Full documentation for the mixer is available at
[RF Blocks](https://rfblocks.org/boards/PE4140B-Quad-FET-Mixer.html)

## License

[CERN-OHL-W v2.](https://ohwr.org/project/cernohl/wikis/Documents/CERN-OHL-version-2)

